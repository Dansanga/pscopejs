var viewport = null;

function init() {
    //debugger
    var container = document.getElementById('iframe-container'),
        viewportOptions = {
            wrapperType: pscope.view.WrapperType.EMBED,
            initValues: 'hello'
        };

    //Interchange commented lines below to toggle POPUP window view and iframe view
    viewport = pscope.defaultViewController.registerViewPort("main",document.getElementById('iframe-container'),"slave.html", viewportOptions);
    //viewport = pscope.defaultViewController.registerViewPort("main",window,"slave.html", {wrapperType: pscope.view.WrapperType.POPUP, initValues: 'hello'});

    viewport.setMessagingListener();
    viewport.msgListener.setActionHandler('click',function(data){
        var message = document.getElementById('message');
        message.value = data;
    }).setActionHandler('recolor',function(color) {
        document.body.style.backgroundColor = color;
        var btn = document.getElementById('send');
        btn.style.color = color;
    });
}

function sendMessage(){
    var data = document.getElementById('data').value;
    viewport.msgListener.sendMessage(data,'click')
}

function recolor(color){
    viewport.msgListener.sendMessage(color,'recolor');
}