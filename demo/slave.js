var msgCfg = null;
function init(){
    msgCfg = pscope.message.registerMessageListener(null,null);
    msgCfg.setActionHandler('click',function(data){
        var message = document.getElementById('message');
        message.value = data;
    }).setActionHandler('recolor',function(color) {
        document.body.style.backgroundColor = color;
        var btn = document.getElementById('send');
        btn.style.color = color;
    });

}

function sendMessage(){
    var data = document.getElementById('data').value;
    msgCfg.sendMessage(data,'click')
}

function recolor(color){
    msgCfg.sendMessage(color,'recolor');
}
