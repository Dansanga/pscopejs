/**
 * @module: pscope/view
 * @author: Daniel Sobrado (daniel.sobrado@gmail.com)
 */

(function (globalNamespace, rootNamespace, moduleNamespace) {
    "use strict";

    //include module within the library namespace
    var rootNS = globalNamespace[rootNamespace],
        moduleNS = rootNS.subNamespace(moduleNamespace);

    //setup enum like structures for option parameters
    moduleNS.ViewType = {};
    moduleNS.WrapperType = {EMBED: 'iframe', POPUP: 'window'};
    moduleNS.DisplayType = {FIXED: 'fixed', MIN: 'min'};

    /**
     * Returns a new ViewController object capable of declaring and manipulating new viewport structures.
     * @returns {{registerViewPort: Function, getViewPortByName: Function}}
     */
    moduleNS.createViewController = function() {

        //closure protected internal elements
        var __viewPorts__ = {}, //list of all ViewPorts managed by this ViewController indexed by name
            /**
             * Creates an Iframe HTML element and appends it as a child node to the received container element or opens a new window
             * associated to the current window. This is an internal function.
             * @param {string} name - Unique identifier to refer to the view
             * @param {HtmlElement} viewContainer - Element that will contain the iframe or window element that will open the new window
             * @param {string} viewUrl - URL to which thew new view will point
             * @param {pscope.view.WrapperType} wrapperType - Inidcates if the view will be of EMBED (iframe) or POPUP (window) type
             * @returns {Window|IFrame} Reference to the HtmlElement that is the visual representation of the ViewPort
             * @private
             */
            __createViewWrapper = function (name, viewContainer, viewUrl, wrapperType){

                var viewWrapper =  null,
                    frameSandbox = [
                        "allow-forms",
                        "allow-popups",
                        "allow-same-origin",
                        "allow-pointer-lock",
                        "allow-scripts"
                    ];

                switch (wrapperType){
                    case moduleNS.WrapperType.EMBED: //for an iframe based embedded ViewPort
                        viewWrapper = document.createElement('iframe');
                        viewWrapper.scrolling = 'no';
                        viewWrapper.id = name;
                        viewWrapper.frameBorder = '0';
                        viewWrapper.marginHeight = '0';
                        viewWrapper.marginWidth = '0';
                        viewWrapper.sandbox = frameSandbox.join(' ');
                        viewWrapper.width = '100%';
                        viewWrapper.height = '100%';
                        viewWrapper.src = viewUrl;
                        viewContainer.appendChild(viewWrapper); //for a window based popup ViewPort
                        break;
                    case moduleNS.WrapperType.POPUP:
                        //Use the "global" object set in the library the library rather than using window directly.
                        viewWrapper = rootNS.global.open(viewUrl, name, "toolbar=yes,scrollbars=yes,resizable=yes");
                        if(!viewWrapper) {
                            //popup blockers will cause this variable to be undefined.
                            throw new Error('Unable to create POPUP viewport. Please check that popups are not being blocked by your browser');
                        }
                        break;
                }

                return viewWrapper;
            };

        //method returns a ViewController object which allows registering and manipulating JS ViewPorts into other applications.
        return {
            /**
             * Creates and initializes a new ViewPort object, adding it to the ViewController's list of active ViewPorts
             * @param {string} name string Unique identifier for the ViewPort
             * @param {HtmlElement} viewContainer HtmlElement Parent HtmlElement that will own/contain the HTML representation of the ViewPort
             * @param {string} viewUrl string URL to which the ViewPort will point.
             * @param {Object} options - Configuration values for the ViewPort             *
             * @param {string} options.wrapperType - Type of ViewPort to create
             * @param {Object} options.initValues - Initialization value
             * @returns {{wrapperType: (*|pscope.view.WrapperType), viewName: *, viewContainer: *, viewWrapper: null, viewWindow: null, viewLocation: null, msgListener: null, initDetails: null, resize: Function, setMessagingListener: Function, initialize: Function}}
             */
            registerViewPort: function (name,viewContainer,viewUrl,options) {
                var viewPort = {
                    wrapperType: options.wrapperType,
                    viewName: name,
                    viewContainer: viewContainer,
                    viewWrapper: null,
                    viewWindow: null,
                    viewLocation: null,
                    msgListener: null,
                    initDetails: null,

                    /**
                     * Resizes the ViewPort's window object to the specified characteristics.
                     * @param {Object} options - Configuration options for resizing the ViewPort {{widthType: pscope.view.DisplayType, width: number, heightType: pscope.view.DisplayType, height: number})
                     * @param {pscope.view.DisplayType} options.widthType - Specifies if the width value is a FIXED size or a MIN size
                     * @param {number} options.width - Number of pixels to configure the width for
                     * @param {pscope.view.DisplayType} options.heightType - Specifies if the height value is a FIXED size or a MIN size
                     * @param {number} options.height - Number of pizels to configure the width for
                     */
                    resize: function (options) {
                        var me = this,
                            widthOffset = 0,
                            heightOffset = 0,
                            widthDim = 0,
                            heightDim = 0;

                        //If the view is an EMBEDded IFrame
                        if(me.wrapperType === moduleNS.WrapperType.EMBED){

                            switch (options.widthType){
                                case moduleNS.DisplayType.FIXED:
                                    me.viewWindow.style.width = options.width + widthOffset + 'px';
                                    break;
                                case moduleNS.DisplayType.MIN:
                                    me.viewWindow.style.minWidth = options.width + widthOffset + 'px';
                                    break;
                            }

                            switch (options.heightType) {
                                case moduleNS.DisplayType.FIXED:
                                    me.viewWindow.style.height = options.height + heightOffset + 'px';
                                    break;
                                case moduleNS.DisplayType.MIN:
                                    me.viewWindow.style.height = rootNS.innerHeight + 'px';
                                    me.viewWindow.style.minHeight = options.height + heightOffset + 'px';
                                    break;
                            }

                        }else { // If the view is a POPUP Window
                            widthDim = me.viewWindow.innerWidth < options.width + widthOffset ? options.width + widthOffset : me.viewWindow.innerWidth;
                            heightDim = me.viewWindow.innerHeight < options.height + heightOffset ? options.height + heightOffset : me.viewWindow.innerHeight;
                            me.viewWindow.resizeTo(widthDim, heightDim);
                        }
                    },

                    /**
                     * Creates a new MessagingListener object that can be used to communicate with the ViewPort contents.
                     * @param {function} [msgListenerFn] - Default function to execute when a message is received
                     * @returns {pscope.Message.MessagingListener} MessagingListener object that can be used to send messages to and react to messages from the ViewPort
                     */
                    setMessagingListener: function (msgListenerFn) {
                        var me = this,
                            listenerFn = (msgListenerFn || rootNS.emptyFn);

                        me.msgListener = rootNS.message.registerMessageListener(listenerFn,me.viewWindow,me.viewLocation);
                        me.msgListener.setActionHandler('resize',function(data){
                            me.resize(data);
                        });

                        return me.msgListener;
                    },

                    /**
                     * Sets or Resets the ViewPort location and establishes communication with the underlying application
                     * @param {string} viewURL - The URL to set the ViewPort for
                     * @param {*} initValues - The data to send to the ViewPort as part of the handshake process.
                     */
                    initialize: function (viewURL, initValues) {
                        var me = this;

                        me.initDetails = initValues;
                        me.viewLocation = viewURL;

                        try {
                            //If the ViewPort does not exist create a new one
                            if (!me.viewWrapper) {
                                me.viewWrapper = __createViewWrapper(me.viewName, me.viewContainer, me.viewLocation, me.wrapperType);
                                me.viewWindow = me.viewWrapper.contentWindow || me.viewWrapper;

                                if (me.wrapperType === moduleNS.WrapperType.POPUP) {
                                    rootNS.global.addEventListener('beforeunload', function () {
                                        me.viewWindow.close();
                                    });
                                }
                            } else { //If the ViewPort already exists re-point to the new url.
                                me.viewWindow.location.assign(me.viewLocation);
                            }

                            //establish actions to perform when the ViewPort document loads
                            me.viewWrapper.onload = function () {
                                if (me.viewWindow.document.readyState !== 'complete') {
                                    throw new Error('View document may not have been fully loaded during handshake process.');
                                }

                                me.viewLocation = me.viewWindow.location.href;

                                me.msgListener = me.msgListener || me.setMessagingListener();
                                me.msgListener.setMessageSource(me.viewLocation);
                                me.msgListener.sendMessage(me.initDetails, 'handshake');
                            };
                        } catch (err){
                            console.log(err);
                        }
                    }
                };

                // add new ViewPort to the list
                __viewPorts__[name] = viewPort;
                // initilize the ViewPort
                viewPort.initialize(viewUrl, options.initValues);
                //return the ViewPort to the caller.
                return viewPort;
            },

            /**
             * Search for the ViewPort from the list by aname
             * @param {string} name - Unique identifier of the desired ViewPort
             * @returns {pscope.view.ViewPort | null} Returns the ViewPort or null if not found.
             */
            getViewPortByName: function (name) {
                return (viewPorts[name] || null);
            }
        };
    };

    //create a default ViewController object at the pscope library root level.
    rootNS.defaultViewController = moduleNS.createViewController();

} (this,'pscope','view'));
