/**
 * @module: pscope/message
 * @author: Daniel Sobrado (daniel.sobrado@gmail.com)
 */

(function (globalNamespace, rootNamespace, moduleNamespace) {
    "use strict";

    //include module within the library namespace
    var rootNS = globalNamespace[rootNamespace],
        moduleNS = rootNS.subNamespace(moduleNamespace),
        __msgListeners__ = [];//closure protected internal element

    __msgListeners__.emptyCount = 0;

    /**
     * Creates a Listener object and adds it to the list of listeners for the library. Returns a MessageListener object to further configure and use the Listener.
     * @param {Function} [processMsgFn] - Optional function to execute whenever a message is received.
     * @param {Window} optMsgSrc - Window object from which to accept messages/send
     * @returns {{setActionTrigger: Function, setActionHandler: Function, setMessageSource: Function, submitAction: Function, sendMessage: Function, deregister: Function}}
     */
    moduleNS.registerMessageListener = function (processMsgFn, optMsgSrc){
        //closure protected internal elements
        var pageParameters = null,
            __listener__ = {
                msgTarget: (optMsgSrc || null), //Window object to send/accept messages
                targetUriOrigin: (optMsgSrc ? optMsgSrc.location.toString() : '*'), //URL to accept messages from
                srcUriOrigin: rootNS.global.location.toString(), //URL of this page
                actionTriggers: {}, //list of action trigger functions that create action type messages
                actionHandlers: {
                    handshakeAutoInit: function (data) {
                        pageParameters = data.params || null;
                    },
                    default: (processMsgFn || rootNS.emptyFn)
                }, //List of action handler functions that react to received action type messages
                /**
                 * Attempts to process received messages, validates handshake messages and executes the default handler.
                 * @param {Object} msg - The message object that was received
                 * @param {string} msg.action - The action type of the received messsage
                 * @param {*} msg.content - The content portion of the received message
                 */
                processingFn: function (msg){
                    var me = this;

                    //Tries to execute a function that matches the action type
                    if(msg.action && me.actionHandlers[msg.action]) {
                        me.actionHandlers[msg.action](msg.content);
                    }

                    // Hardcoded check for handshake messages
                    if(msg.action === 'handshake'){
                        me.actionHandlers.handshakeAutoInit(msg.content);
                    }

                    //Executes default handler if available
                    me.actionHandlers.default(msg.content || msg);
                }
            },
            /**
             * Creates a new message of the specified type with the provided content
             * @param {string} msgAction
             * @param {*} msgContent
             * @returns {{source: *, target: *, trigger: null, action: *, content: *}}
             * @private
             */
            __createMessage = function (msgAction, msgContent) {
                return {
                    source: __listener__.srcUriOrigin,
                    target: __listener__.targetUriOrigin,
                    trigger: null,
                    action: msgAction,
                    content: msgContent
                };
            };

        //New Listener is added to the list of listeners
        __msgListeners__.push(__listener__);

        //keep track of listeners with no target. Only one can exist and only if there are no more listeners.
        if(!__listener__.msgTarget){
            __msgListeners__.emptyCount += 1;
        }

        //assign existing value or new function if currently undefined.
        rootNS.global.onmessage = (window.onmessage || function (evt) {
            if(__msgListeners__.length > 1 && __msgListeners__.emptyCount > 0) {
                throw new Error(rootNS.fullName + ': Anonymous messaging listener detected on a multiple listener configuration');
            }else{
                //this is the code that makes the messaging magic work
                __msgListeners__.forEach(function (line) {
                    line.msgTarget = (line.msgTarget || evt.source);
                    if((evt.source === line.msgTarget) && (line.targetUriOrigin === '*' || line.targetUriOrigin === evt.source.location.toString())) {
                        line.processingFn(evt.data);
                        if(line.targetUriOrigin === '*'){
                            line.targetUriOrigin = (!evt.source) ? evt.data.source : evt.source.location.toString();
                            if(line.targetUriOrigin !== '*'){
                                __msgListeners__.emptyCount -= 1;
                            }
                        }
                    }
                });
            }
        });

        return {
            /**
             * Defines a function to execute in order to produce a message of a specific type
             * @param {string} actionName - Action type of the message to produce
             * @param (Function) produceMsgFn - Function that produces the message of the specified type.
             * @returns {pscope.message.MessagingListener}
             */
            setActionTrigger: function (actionName, produceMsgFn) {
                __listener__.actionTriggers[actionName] = produceMsgFn;
                return this;
            },
            /**
             * Defines a function to execute when a message of a specific type is received
             * @param {string} actionName - Action type of the message to handle
             * @param {Function} processActionFn - Function to execute when a message of the specified type is received
             * @returns {pscope.message.MessagingListener}
             */
            setActionHandler: function (actionName, processActionFn) {
                __listener__.actionHandlers[actionName] = processActionFn;
                return this;
            },
            /**
             * Changes the url to send/receive messages
             * @param {string} msgSrc - New URL to send/receive messages.
             * @returns {pscope.message.MessagingListener}
             */
            setMessageSource: function (msgSrc) {
                __listener__.targetUriOrigin = msgSrc;
                return this;
            },
            /**
             * Executes a trigger function, constructs a message of that type with the returned content and sends the resulting message to the messaging target.
             * @param {string} actionName - Identifier of the trigger to execute and resulting message type.
             */
            submitAction: function (actionName) {
                if(!__listener__.msgTarget || !__listener__.actionTriggers[actionName]){
                    throw new ReferenceError("Cross-document messaging not properly defined. Message target or requested action handler may be missing.");
                }else{
                    var triggerFn = __listener__.actionTriggers[actionName],
                        message = __createMessage(actionName,triggerFn());

                    message.trigger = (message.trigger || actionName);
                    __listener__.msgTarget.postMessage(message, __listener__.targetUriOrigin);
                }
            },
            /**
             * Constructs and sends a message of the specified action type and content to the messaging target
             * @param {*} msgContent - Data to send in the message
             * @param (string) msgAction - Message action type
             */
            sendMessage: function (msgContent, msgAction) {
                if(!__listener__.msgTarget){
                    throw new ReferenceError("Cross-document messaging not properly defined. Message target or requested action handler may be missing.");
                }else{
                    __listener__.msgTarget.postMessage(__createMessage(msgAction, msgContent), __listener__.targetUriOrigin);
                }
            },
            /**
             * Removes the underlying Listener object from the list of listeners
             * @returns {boolean} Indicator of removal success.
             */
            deregister: function () {
                var listenerIndex = __msgListeners__.indexOf(__listener__),
                    removedObjects = null;

                if(listenerIndex > -1) {
                    removedObjects = __msgListeners__.splice(listenerIndex, 1);
                    if((!removedObjects[0].msgTarget || removedObjects[0].targetUriOrigin === '*') && __msgListeners__.emptyCount > 0) {
                        __msgListeners__.emptyCount -= 1;
                    }
                    return true;
                }else{
                    return false;
                }
            }
        };
    };
} (this,'pscope','message'));