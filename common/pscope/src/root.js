/**
 * PScopeJS 2015
 * @module: pscope/root
 * @author: Daniel Sobrado (daniel.sobrado@gmail.com)
 * @location: Heredia, Costa Rica
 *
 * //TODO: add OpnSrc license information
 *
 */
(function (globalNamespace, moduleNamespace) {
    "use strict";

    /**
     * Top level namespace object for the library.
     * @type {{global: Window, version: string, releaseState: string, fullName: string, namespace: string, emptyFn: Function, subNamespace: Function}}
     */
    globalNamespace[moduleNamespace] = {
        global: globalNamespace,
        version: '0.0.1',
        releaseState: 'alpha',
        fullName: 'PScopeJS',
        namespace: moduleNamespace,
        emptyFn: function () {},
        /**
         * Creates or finds and returns namespace objects
         * @param {string} namespaceName Period separated namespace chain.
         * @returns {object} Object from last sub-namespace in the chain
         */
        subNamespace: function (namespaceName) {
            //remove whitespace characters, separate into namespace parts by period character and iterate
            return namespaceName.replace(/\s/g ,'').split('.').reduce(function (parent, newNamespace) {
                //new or pre-existing sub-namespace object is passed on for valid identifiers. Invalid ones are skipped.
                return (newNamespace ? (parent[newNamespace] = parent[newNamespace] || {}) : parent);
            },this);
        }
    };

}) (this, 'pscope');