/**
 * PScopeJS 2015
 * @module: pscope/root
 * @author: Daniel Sobrado (daniel.sobrado@gmail.com)
 * @location: Heredia, Costa Rica
 *
 * //TODO: add OpnSrc license information
 *
 */
(function (globalNamespace, moduleNamespace) {
    "use strict";

    /**
     * Top level namespace object for the library.
     * @type {{global: Window, version: string, releaseState: string, fullName: string, namespace: string, emptyFn: Function, subNamespace: Function}}
     */
    globalNamespace[moduleNamespace] = {
        global: globalNamespace,
        version: '0.0.1',
        releaseState: 'alpha',
        fullName: 'PScopeJS',
        namespace: moduleNamespace,
        emptyFn: function () {},
        /**
         * Creates or finds and returns namespace objects
         * @param {string} namespaceName Period separated namespace chain.
         * @returns {object} Object from last sub-namespace in the chain
         */
        subNamespace: function (namespaceName) {
            //remove whitespace characters, separate into namespace parts by period character and iterate
            return namespaceName.replace(/\s/g ,'').split('.').reduce(function (parent, newNamespace) {
                //new or pre-existing sub-namespace object is passed on for valid identifiers. Invalid ones are skipped.
                return (newNamespace ? (parent[newNamespace] = parent[newNamespace] || {}) : parent);
            },this);
        }
    };

}) (this, 'pscope');

/**
 * @module: pscope/message
 * @author: Daniel Sobrado (daniel.sobrado@gmail.com)
 */

(function (globalNamespace, rootNamespace, moduleNamespace) {
    "use strict";

    //include module within the library namespace
    var rootNS = globalNamespace[rootNamespace],
        moduleNS = rootNS.subNamespace(moduleNamespace),
        __msgListeners__ = [];//closure protected internal element

    __msgListeners__.emptyCount = 0;

    /**
     * Creates a Listener object and adds it to the list of listeners for the library. Returns a MessageListener object to further configure and use the Listener.
     * @param {Function} [processMsgFn] - Optional function to execute whenever a message is received.
     * @param {Window} optMsgSrc - Window object from which to accept messages/send
     * @returns {{setActionTrigger: Function, setActionHandler: Function, setMessageSource: Function, submitAction: Function, sendMessage: Function, deregister: Function}}
     */
    moduleNS.registerMessageListener = function (processMsgFn, optMsgSrc){
        //closure protected internal elements
        var pageParameters = null,
            __listener__ = {
                msgTarget: (optMsgSrc || null), //Window object to send/accept messages
                targetUriOrigin: (optMsgSrc ? optMsgSrc.location.toString() : '*'), //URL to accept messages from
                srcUriOrigin: rootNS.global.location.toString(), //URL of this page
                actionTriggers: {}, //list of action trigger functions that create action type messages
                actionHandlers: {
                    handshakeAutoInit: function (data) {
                        pageParameters = data.params || null;
                    },
                    default: (processMsgFn || rootNS.emptyFn)
                }, //List of action handler functions that react to received action type messages
                /**
                 * Attempts to process received messages, validates handshake messages and executes the default handler.
                 * @param {Object} msg - The message object that was received
                 * @param {string} msg.action - The action type of the received messsage
                 * @param {*} msg.content - The content portion of the received message
                 */
                processingFn: function (msg){
                    var me = this;

                    //Tries to execute a function that matches the action type
                    if(msg.action && me.actionHandlers[msg.action]) {
                        me.actionHandlers[msg.action](msg.content);
                    }

                    // Hardcoded check for handshake messages
                    if(msg.action === 'handshake'){
                        me.actionHandlers.handshakeAutoInit(msg.content);
                    }

                    //Executes default handler if available
                    me.actionHandlers.default(msg.content || msg);
                }
            },
            /**
             * Creates a new message of the specified type with the provided content
             * @param {string} msgAction
             * @param {*} msgContent
             * @returns {{source: *, target: *, trigger: null, action: *, content: *}}
             * @private
             */
            __createMessage = function (msgAction, msgContent) {
                return {
                    source: __listener__.srcUriOrigin,
                    target: __listener__.targetUriOrigin,
                    trigger: null,
                    action: msgAction,
                    content: msgContent
                };
            };

        //New Listener is added to the list of listeners
        __msgListeners__.push(__listener__);

        //keep track of listeners with no target. Only one can exist and only if there are no more listeners.
        if(!__listener__.msgTarget){
            __msgListeners__.emptyCount += 1;
        }

        //assign existing value or new function if currently undefined.
        rootNS.global.onmessage = (window.onmessage || function (evt) {
            if(__msgListeners__.length > 1 && __msgListeners__.emptyCount > 0) {
                throw new Error(rootNS.fullName + ': Anonymous messaging listener detected on a multiple listener configuration');
            }else{
                //this is the code that makes the messaging magic work
                __msgListeners__.forEach(function (line) {
                    line.msgTarget = (line.msgTarget || evt.source);
                    if((evt.source === line.msgTarget) && (line.targetUriOrigin === '*' || line.targetUriOrigin === evt.source.location.toString())) {
                        line.processingFn(evt.data);
                        if(line.targetUriOrigin === '*'){
                            line.targetUriOrigin = (!evt.source) ? evt.data.source : evt.source.location.toString();
                            if(line.targetUriOrigin !== '*'){
                                __msgListeners__.emptyCount -= 1;
                            }
                        }
                    }
                });
            }
        });

        return {
            /**
             * Defines a function to execute in order to produce a message of a specific type
             * @param {string} actionName - Action type of the message to produce
             * @param (Function) produceMsgFn - Function that produces the message of the specified type.
             * @returns {pscope.message.MessagingListener}
             */
            setActionTrigger: function (actionName, produceMsgFn) {
                __listener__.actionTriggers[actionName] = produceMsgFn;
                return this;
            },
            /**
             * Defines a function to execute when a message of a specific type is received
             * @param {string} actionName - Action type of the message to handle
             * @param {Function} processActionFn - Function to execute when a message of the specified type is received
             * @returns {pscope.message.MessagingListener}
             */
            setActionHandler: function (actionName, processActionFn) {
                __listener__.actionHandlers[actionName] = processActionFn;
                return this;
            },
            /**
             * Changes the url to send/receive messages
             * @param {string} msgSrc - New URL to send/receive messages.
             * @returns {pscope.message.MessagingListener}
             */
            setMessageSource: function (msgSrc) {
                __listener__.targetUriOrigin = msgSrc;
                return this;
            },
            /**
             * Executes a trigger function, constructs a message of that type with the returned content and sends the resulting message to the messaging target.
             * @param {string} actionName - Identifier of the trigger to execute and resulting message type.
             */
            submitAction: function (actionName) {
                if(!__listener__.msgTarget || !__listener__.actionTriggers[actionName]){
                    throw new ReferenceError("Cross-document messaging not properly defined. Message target or requested action handler may be missing.");
                }else{
                    var triggerFn = __listener__.actionTriggers[actionName],
                        message = __createMessage(actionName,triggerFn());

                    message.trigger = (message.trigger || actionName);
                    __listener__.msgTarget.postMessage(message, __listener__.targetUriOrigin);
                }
            },
            /**
             * Constructs and sends a message of the specified action type and content to the messaging target
             * @param {*} msgContent - Data to send in the message
             * @param (string) msgAction - Message action type
             */
            sendMessage: function (msgContent, msgAction) {
                if(!__listener__.msgTarget){
                    throw new ReferenceError("Cross-document messaging not properly defined. Message target or requested action handler may be missing.");
                }else{
                    __listener__.msgTarget.postMessage(__createMessage(msgAction, msgContent), __listener__.targetUriOrigin);
                }
            },
            /**
             * Removes the underlying Listener object from the list of listeners
             * @returns {boolean} Indicator of removal success.
             */
            deregister: function () {
                var listenerIndex = __msgListeners__.indexOf(__listener__),
                    removedObjects = null;

                if(listenerIndex > -1) {
                    removedObjects = __msgListeners__.splice(listenerIndex, 1);
                    if((!removedObjects[0].msgTarget || removedObjects[0].targetUriOrigin === '*') && __msgListeners__.emptyCount > 0) {
                        __msgListeners__.emptyCount -= 1;
                    }
                    return true;
                }else{
                    return false;
                }
            }
        };
    };
} (this,'pscope','message'));

/**
 * @module: pscope/view
 * @author: Daniel Sobrado (daniel.sobrado@gmail.com)
 */

(function (globalNamespace, rootNamespace, moduleNamespace) {
    "use strict";

    //include module within the library namespace
    var rootNS = globalNamespace[rootNamespace],
        moduleNS = rootNS.subNamespace(moduleNamespace);

    //setup enum like structures for option parameters
    moduleNS.ViewType = {};
    moduleNS.WrapperType = {EMBED: 'iframe', POPUP: 'window'};
    moduleNS.DisplayType = {FIXED: 'fixed', MIN: 'min'};

    /**
     * Returns a new ViewController object capable of declaring and manipulating new viewport structures.
     * @returns {{registerViewPort: Function, getViewPortByName: Function}}
     */
    moduleNS.createViewController = function() {

        //closure protected internal elements
        var __viewPorts__ = {}, //list of all ViewPorts managed by this ViewController indexed by name
            /**
             * Creates an Iframe HTML element and appends it as a child node to the received container element or opens a new window
             * associated to the current window. This is an internal function.
             * @param {string} name - Unique identifier to refer to the view
             * @param {HtmlElement} viewContainer - Element that will contain the iframe or window element that will open the new window
             * @param {string} viewUrl - URL to which thew new view will point
             * @param {pscope.view.WrapperType} wrapperType - Inidcates if the view will be of EMBED (iframe) or POPUP (window) type
             * @returns {Window|IFrame} Reference to the HtmlElement that is the visual representation of the ViewPort
             * @private
             */
            __createViewWrapper = function (name, viewContainer, viewUrl, wrapperType){

                var viewWrapper =  null,
                    frameSandbox = [
                        "allow-forms",
                        "allow-popups",
                        "allow-same-origin",
                        "allow-pointer-lock",
                        "allow-scripts"
                    ];

                switch (wrapperType){
                    case moduleNS.WrapperType.EMBED: //for an iframe based embedded ViewPort
                        viewWrapper = document.createElement('iframe');
                        viewWrapper.scrolling = 'no';
                        viewWrapper.id = name;
                        viewWrapper.frameBorder = '0';
                        viewWrapper.marginHeight = '0';
                        viewWrapper.marginWidth = '0';
                        viewWrapper.sandbox = frameSandbox.join(' ');
                        viewWrapper.width = '100%';
                        viewWrapper.height = '100%';
                        viewWrapper.src = viewUrl;
                        viewContainer.appendChild(viewWrapper); //for a window based popup ViewPort
                        break;
                    case moduleNS.WrapperType.POPUP:
                        //Use the "global" object set in the library the library rather than using window directly.
                        viewWrapper = rootNS.global.open(viewUrl, name, "toolbar=yes,scrollbars=yes,resizable=yes");
                        if(!viewWrapper) {
                            //popup blockers will cause this variable to be undefined.
                            throw new Error('Unable to create POPUP viewport. Please check that popups are not being blocked by your browser');
                        }
                        break;
                }

                return viewWrapper;
            };

        //method returns a ViewController object which allows registering and manipulating JS ViewPorts into other applications.
        return {
            /**
             * Creates and initializes a new ViewPort object, adding it to the ViewController's list of active ViewPorts
             * @param {string} name string Unique identifier for the ViewPort
             * @param {HtmlElement} viewContainer HtmlElement Parent HtmlElement that will own/contain the HTML representation of the ViewPort
             * @param {string} viewUrl string URL to which the ViewPort will point.
             * @param {Object} options - Configuration values for the ViewPort             *
             * @param {string} options.wrapperType - Type of ViewPort to create
             * @param {Object} options.initValues - Initialization value
             * @returns {{wrapperType: (*|pscope.view.WrapperType), viewName: *, viewContainer: *, viewWrapper: null, viewWindow: null, viewLocation: null, msgListener: null, initDetails: null, resize: Function, setMessagingListener: Function, initialize: Function}}
             */
            registerViewPort: function (name,viewContainer,viewUrl,options) {
                var viewPort = {
                    wrapperType: options.wrapperType,
                    viewName: name,
                    viewContainer: viewContainer,
                    viewWrapper: null,
                    viewWindow: null,
                    viewLocation: null,
                    msgListener: null,
                    initDetails: null,

                    /**
                     * Resizes the ViewPort's window object to the specified characteristics.
                     * @param {Object} options - Configuration options for resizing the ViewPort {{widthType: pscope.view.DisplayType, width: number, heightType: pscope.view.DisplayType, height: number})
                     * @param {pscope.view.DisplayType} options.widthType - Specifies if the width value is a FIXED size or a MIN size
                     * @param {number} options.width - Number of pixels to configure the width for
                     * @param {pscope.view.DisplayType} options.heightType - Specifies if the height value is a FIXED size or a MIN size
                     * @param {number} options.height - Number of pizels to configure the width for
                     */
                    resize: function (options) {
                        var me = this,
                            widthOffset = 0,
                            heightOffset = 0,
                            widthDim = 0,
                            heightDim = 0;

                        //If the view is an EMBEDded IFrame
                        if(me.wrapperType === moduleNS.WrapperType.EMBED){

                            switch (options.widthType){
                                case moduleNS.DisplayType.FIXED:
                                    me.viewWindow.style.width = options.width + widthOffset + 'px';
                                    break;
                                case moduleNS.DisplayType.MIN:
                                    me.viewWindow.style.minWidth = options.width + widthOffset + 'px';
                                    break;
                            }

                            switch (options.heightType) {
                                case moduleNS.DisplayType.FIXED:
                                    me.viewWindow.style.height = options.height + heightOffset + 'px';
                                    break;
                                case moduleNS.DisplayType.MIN:
                                    me.viewWindow.style.height = rootNS.innerHeight + 'px';
                                    me.viewWindow.style.minHeight = options.height + heightOffset + 'px';
                                    break;
                            }

                        }else { // If the view is a POPUP Window
                            widthDim = me.viewWindow.innerWidth < options.width + widthOffset ? options.width + widthOffset : me.viewWindow.innerWidth;
                            heightDim = me.viewWindow.innerHeight < options.height + heightOffset ? options.height + heightOffset : me.viewWindow.innerHeight;
                            me.viewWindow.resizeTo(widthDim, heightDim);
                        }
                    },

                    /**
                     * Creates a new MessagingListener object that can be used to communicate with the ViewPort contents.
                     * @param {function} [msgListenerFn] - Default function to execute when a message is received
                     * @returns {pscope.Message.MessagingListener} MessagingListener object that can be used to send messages to and react to messages from the ViewPort
                     */
                    setMessagingListener: function (msgListenerFn) {
                        var me = this,
                            listenerFn = (msgListenerFn || rootNS.emptyFn);

                        me.msgListener = rootNS.message.registerMessageListener(listenerFn,me.viewWindow,me.viewLocation);
                        me.msgListener.setActionHandler('resize',function(data){
                            me.resize(data);
                        });

                        return me.msgListener;
                    },

                    /**
                     * Sets or Resets the ViewPort location and establishes communication with the underlying application
                     * @param {string} viewURL - The URL to set the ViewPort for
                     * @param {*} initValues - The data to send to the ViewPort as part of the handshake process.
                     */
                    initialize: function (viewURL, initValues) {
                        var me = this;

                        me.initDetails = initValues;
                        me.viewLocation = viewURL;

                        try {
                            //If the ViewPort does not exist create a new one
                            if (!me.viewWrapper) {
                                me.viewWrapper = __createViewWrapper(me.viewName, me.viewContainer, me.viewLocation, me.wrapperType);
                                me.viewWindow = me.viewWrapper.contentWindow || me.viewWrapper;

                                if (me.wrapperType === moduleNS.WrapperType.POPUP) {
                                    rootNS.global.addEventListener('beforeunload', function () {
                                        me.viewWindow.close();
                                    });
                                }
                            } else { //If the ViewPort already exists re-point to the new url.
                                me.viewWindow.location.assign(me.viewLocation);
                            }

                            //establish actions to perform when the ViewPort document loads
                            me.viewWrapper.onload = function () {
                                if (me.viewWindow.document.readyState !== 'complete') {
                                    throw new Error('View document may not have been fully loaded during handshake process.');
                                }

                                me.viewLocation = me.viewWindow.location.href;

                                me.msgListener = me.msgListener || me.setMessagingListener();
                                me.msgListener.setMessageSource(me.viewLocation);
                                me.msgListener.sendMessage(me.initDetails, 'handshake');
                            };
                        } catch (err){
                            console.log(err);
                        }
                    }
                };

                // add new ViewPort to the list
                __viewPorts__[name] = viewPort;
                // initilize the ViewPort
                viewPort.initialize(viewUrl, options.initValues);
                //return the ViewPort to the caller.
                return viewPort;
            },

            /**
             * Search for the ViewPort from the list by aname
             * @param {string} name - Unique identifier of the desired ViewPort
             * @returns {pscope.view.ViewPort | null} Returns the ViewPort or null if not found.
             */
            getViewPortByName: function (name) {
                return (viewPorts[name] || null);
            }
        };
    };

    //create a default ViewController object at the pscope library root level.
    rootNS.defaultViewController = moduleNS.createViewController();

} (this,'pscope','view'));
